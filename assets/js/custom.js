$(document).ready(function () {
    $(".topnavmnu").removeClass('current-menu-parent');
    var parts = window.location.pathname.split("/");
    if(parts[1] == ""){
        parts[1] = "home";
    }
    var menu_item = parts[1];
    $("."+menu_item).addClass('current-menu-parent');

    if(menu_item == "booking")
    {
        $(".tours").addClass('current-menu-parent');
        $(".cntrl").attr('disabled', true);
    }

});
