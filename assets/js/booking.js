$(document).ready(function () {

    $("#lnk_view_tour_details").click(function(evt) {
        evt.preventDefault();
        var selected_tour = $("#sel_tours").val();
        window.location = "/tours/view_tour/"+selected_tour;
    });


    $("#btn_subscribe_add").click(function(evt) {
        evt.preventDefault();
        var email = $("#email_subscriber_fld").val();

        if(validateEmail(email)){

            var formdata = [];
            formdata.push({name: "semail", value: email});
            $.post('/booking/subscribe', formdata);
            $("#subscriber_success_message").removeClass("alert-error alert-success").addClass("alert-success");
            $("#subscribe_heading").html("Subscribe Message");
            $("#subscribe_msg").html("Congratulations your email address has been added to our subscriber list you will receive the latest email marketing from Ziwa Tours to unsubscribe please email info@ziwatours.biz or use the unsubscribe link included in all emails sent out ");
            $("#email_subscriber_fld").val("");
        } else {
            $("#subscriber_success_message").removeClass("alert-error alert-success").addClass("alert-error");
            $("#subscribe_heading").html("Subscribe Message");
            $("#subscribe_msg").html("Error occurred your email address has not been added to our subscriber list if problem persists please email info@ziwatours.biz or use the unsubscribe link included in all emails sent out ");
        }
        $("#subscriber_success_message").show();

    });

    function validateEmail(email){
        var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        var valid = emailReg.test(email);

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }

    $("#btn_private_driver_booking").click(function(evt) {
        evt.preventDefault();
        $("#private_success_message").hide();
        $("#private_error_message").hide();

        if(($("#private_name").val() == "")||($("#private_email").val() == "")||($("#private_date").val() == ""))
        {
            $("#private_error_message").show();
        }
        else
        {

            var private_booking_form_data = $("#private_chauffer_frm").serializeArray();
            private_booking_form_data.push({name: "booking_type", value: "1"});
            private_booking_form_data.push({name: "booking_id", value: "1"});

             $.post('/booking/request_private_booking', private_booking_form_data, function(data){
                 console.log("Data : "+data);
                 if(data != 0) {
                    $("#psm_name").html($("#private_name").val());
                    $("#psm_email").html($("#private_email").val());
                    $('#private_chauffer_frm').trigger("reset");
                    $("#private_success_message").show();
                 }else {
                    var msg = "A booking process error occured please try again if problem persists please contact info@ziwatours.biz";
                    $("#pem_msg").html(msg);
                    $("#private_error_message").show();
                 }

             }).fail(function() {
                var msg = "An booking connection error occured please try again if problem persists please contact info@ziwatours.biz";
                $("#pem_msg").html(msg);
                $("#private_error_message").show();
             });

            //console.log(private_booking_form_data);



        }
    });

    $("#btn_book_package").click(function(evt) {
        evt.preventDefault();
        $("#package_success_message").hide();
        $("#package_error_message").hide();

        if(($("#package_name").val() == "")||($("#package_email").val() == "")||($("#package_date").val() == "")||($("#package_selected").val() == 0))
        {
            $("#package_error_message").show();
        }
        else
        {
            var package_booking_form_data = $("#package_frm").serializeArray();
            package_booking_form_data.push({name: "booking_type", value: "2"});
            package_booking_form_data.push({name: "booking_id", value: $("#package_selected").val()});
            //console.log(package_booking_form_data);


            $.post('/booking/request_package_booking', package_booking_form_data, function(data){

                console.log("Data : "+data);
                if(data != 0) {
                    $("#pksm_name").html($("#package_name").val());
                    $("#pksm_email").html($("#package_email").val());
                    $('#package_frm').trigger("reset");
                    $("#package_success_message").show();
                }else {
                    var msg = "A booking process error occured please try again if problem persists please contact info@ziwatours.biz";
                    $("#pkem_msg").html(msg);
                    $("#package_error_message").show();
                }

            }).fail(function() {
                var msg = "An booking connection error occured please try again if problem persists please contact info@ziwatours.biz";
                $("#pkem_msg").html(msg);
                $("#package_error_message").show();
            });

        }
    });

    $("#request_tour_booking_link").click(function(evt) {
        evt.preventDefault();
        // /booking/confirm_booking
        var tour_id = $("#tour_id").val();
        var firstname = $("#firstname").val();
        var email = $("#email").val();
        var bookdate = $("#bookdate").val();
        var bookcode = $("#bookcode").val();
        if((firstname.length > 0)&&(email.length > 0)&&(bookdate.length > 0))
        {
            var formdata = [];
            formdata.push({name: "tour_id", value: tour_id});
            formdata.push({name: "tour_name", value: firstname});
            formdata.push({name: "tour_email", value: email});
            formdata.push({name: "tour_date", value: bookdate});
            formdata.push({name: "tour_code", value: bookcode});
            formdata.push({name: "booking_type", value: "3"});
            $.post('/booking/request_tour_booking', formdata, function(data){
                if(data != 0) {
                    $("#booking_message_block").removeClass("alert-error alert-success").addClass("alert-success");
                    var url_complete = "/booking/confirm_booking/"+$("#tour_id").val();
                    setTimeout(function(){ window.location = url_complete; }, 1500);
                }else {
                    $("#booking_message_block").removeClass("alert-error alert-success").addClass("alert-error");
                }

            }).fail(function() {
                $("#booking_message_block").removeClass("alert-error alert-success").addClass("alert-error");
            });
        }
        else
        {
            $("#booking_message_block").removeClass("alert-error alert-success").addClass("alert-error");
        }


    });


    $("#btn_exclusive_booking_request").click(function(evt) {
        evt.preventDefault();
        $("#exclusive_success_message").hide();
        $("#exclusive_error_message").hide();
        if(($("#exclusive_name").val() == "")||($("#exclusive_email").val() == "")||($("#exclusive_date").val() == "")||($("#exclusive_select").val() == 0))
        {
            $("#exclusive_error_message").show();
        }
        else
        {
            var exclusive_booking_form_data = $("#exclusive_frm").serializeArray();
            exclusive_booking_form_data.push({name: "booking_type", value: "4"});
            exclusive_booking_form_data.push({name: "booking_id", value: $("#exclusive_select").val()});
            //console.log(package_booking_form_data);

            $.post('/booking/request_exclusive_booking', exclusive_booking_form_data, function(data){

                console.log("Data : "+data);
                if(data != 0) {
                    $("#xsm_name").html($("#exclusive_name").val());
                    $("#xsm_email").html($("#exclusive_email").val());
                    $('#exclusive_frm').trigger("reset");
                    $("#exclusive_success_message").show();
                }else {
                    var msg = "A booking process error occured please try again if problem persists please contact info@ziwatours.biz";
                    $("#xem_msg").html(msg);
                    $("#exclusive_error_message").show();
                }

            }).fail(function() {
                var msg = "An booking connection error occured please try again if problem persists please contact info@ziwatours.biz";
                $("#xem_msg").html(msg);
                $("#exclusive_error_message").show();
            });


        }
    });


});
