<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
    }

	public function index()
	{
		$data['layout'] = 'mobi';
        $data['main_content'] = '/about/index';
        $this->load->view('includes/template',$data);
	}

    public function aboutsa()
    {
        $data['layout'] = 'mobi';
        $data['main_content'] = '/about/aboutsa';
        $this->load->view('includes/template',$data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */