<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Enquire extends CI_Controller {

    function __construct()
    {
       parent::__construct();
       $this->load->model('booking_model','enquiry');
    }

	public function index()
	{
		$data['layout'] = 'mobi';
        $data['main_content'] = '/enquire/index';
        $this->load->view('includes/template',$data);
	}

    public function doenquiry()
    {
        $enquiry_data = $this->input->post(NULL, TRUE);
        if(count($enquiry_data) == 5)
        {
            $subject = $enquiry_data['subject'];
            $ref = $enquiry_data['ref'];
            $name = $enquiry_data['name'];
            $email = $enquiry_data['email'];
            $message = $enquiry_data['message'];

            $this->enquiry->send_enquiry($subject,$ref,$name,$email,$message);
            // do some things and its all good
            echo "success";
        }
        else
        {
            die("Bad Request if problem persists please email info@ziwatours.biz");
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */