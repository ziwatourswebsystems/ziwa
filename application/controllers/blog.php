<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
        $this->load->model('blog_model','blog');

    }

	public function index($blog_id = 0)
	{
        $data['blog_index'] = $blog_id;
        $data['blog'] = $this->blog->get_all_blogs();
		$data['layout'] = 'mobi';
        $data['main_content'] = '/blog/index';
        $this->load->view('includes/template',$data);
	}

    public function view_blog($blog_id = 0)
    {
        if($blog_id == 0)
        {
            redirect('/blog/', 'refresh');
        }
        $data['blog'] = $this->blog->get_all_blogs();
        $data['blogs'] = $this->blog->get_blog_details($blog_id);
        $data['layout'] = 'mobi';
        $data['main_content'] = '/blog/view_blog';
        $this->load->view('includes/template',$data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */