<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
        $this->load->model('services_model','services');

    }

	public function index()
	{
        $data['services'] = $this->services->get_all_services();
		$data['layout'] = 'mobi';
        $data['main_content'] = '/services/index';
        $this->load->view('includes/template',$data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */