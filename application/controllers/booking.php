<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
        $this->load->model('auth_model','user');
        $this->load->model('booking_model','booking');
        $this->load->model('tour_model','tour');

    }

	public function index($tour_id = 0)
	{
        if($tour_id != 0)
        {
            $data['tour'] = $this->tour->get_tour_details($tour_id);
        }
        $data['tour_id'] = $tour_id;
		$data['layout'] = 'mobi';
        $data['main_content'] = '/booking/index';
        $this->load->view('includes/template',$data);
	}

    public function confirm_booking($tour_id = null)
    {
        if($tour_id != 0)
        {
            $data['tour'] = $this->tour->get_tour_details($tour_id);
        }
        $data['layout'] = 'mobi';
        $data['main_content'] = '/booking/complete_booking';
        $this->load->view('includes/template',$data);
    }

    public function subscribe()
    {
        $subscribe_data = $this->input->post(NULL, TRUE);
        if(count($subscribe_data) == 1)
        {
            $email = $subscribe_data['semail'];
            $this->booking->add_subscriber($email);
        }
    }

    public function request_private_booking()
    {
        $booking_data = $this->input->post(NULL, TRUE);
        if(count($booking_data) == 5)
        {
            $bname = $booking_data['private_name'];
            $bemail = $booking_data['private_email'];
            $bdate = $booking_data['private_date'];
            $booking_type = $booking_data['booking_type'];
            $booking_id = $booking_data['booking_id'];
            //print_r($booking_data);
            $user_info = $this->user->check_user($bemail,$bname);
            if($user_info['user_email'])
            {
                $this->booking->create_booking_request($bname,$bemail,$booking_type,$booking_id,$bdate);
            }
            echo "success";

        }
        else
        {
            die("Bad Request if problem persists please email info@ziwatours.biz");
        }
    }

    public function request_package_booking()
    {
        $booking_data = $this->input->post(NULL, TRUE);
        if(count($booking_data) == 6)
        {
            $bname = $booking_data['package_name'];
            $bemail = $booking_data['package_email'];
            $bdate = $booking_data['package_date'];
            $booking_type = $booking_data['booking_type'];
            $booking_id = $booking_data['booking_id'];
            //print_r($booking_data);
            $user_info = $this->user->check_user($bemail,$bname);
            if($user_info['user_email'])
            {
                $this->booking->create_booking_request($bname,$bemail,$booking_type,$booking_id,$bdate);
            }
            echo "success";
        }
        else
        {
            print_r(count($booking_data));
            die("   Bad Request if problem persists please email info@ziwatours.biz");
        }
    }

    public function request_tour_booking()
    {
         $booking_data = $this->input->post(NULL, TRUE);

        if(count($booking_data) == 6)
        {
            $tour_email = $booking_data['tour_email'];
            $tour_name = $booking_data['tour_name'];
            $tour_date = $booking_data['tour_date'];
            $booking_type = $booking_data['booking_type'];
            $booking_id = $booking_data['tour_id'];
            //print_r($booking_data);
            $user_info = $this->user->check_user($tour_email,$tour_name);
            if($user_info['user_email'])
            {
                $this->booking->create_booking_request($tour_name,$tour_email,$booking_type,$booking_id,$tour_date);
            }
            echo "success";
        }
        else
        {
            print_r(count($booking_data));
            die("   Bad Request if problem persists please email info@ziwatours.biz");
        }
    }

    public function request_exclusive_booking()
    {
        $booking_data = $this->input->post(NULL, TRUE);
        if(count($booking_data) == 6)
        {
            $bname = $booking_data['exclusive_name'];
            $bemail = $booking_data['exclusive_email'];
            $bdate = $booking_data['exclusive_date'];
            $booking_type = $booking_data['booking_type'];
            $booking_id = $booking_data['booking_id'];
            //print_r($booking_data);
            $user_info = $this->user->check_user($bemail,$bname);
            if($user_info['user_email'])
            {
                $this->booking->create_booking_request($bname,$bemail,$booking_type,$booking_id,$bdate);
            }
            echo "success";
        }
        else
        {
            print_r(count($booking_data));
            die("   Bad Request if problem persists please email info@ziwatours.biz");
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */