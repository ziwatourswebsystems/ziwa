<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
    }

	public function index()
	{
        $this->load->helper('directory');
        $data['images_map'] = directory_map('assets/images/gallery/', FALSE, TRUE);
		$data['layout'] = 'mobi';
        $data['main_content'] = '/gallery/index';
        $this->load->view('includes/template',$data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */