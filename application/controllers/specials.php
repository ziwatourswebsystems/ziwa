<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Specials extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
        $this->load->model('home_model','home');

    }

	public function index()
	{
        $data['testimonials'] = $this->home->get_all_testimonials();
        $data['specials'] = $this->home->get_all_specials();

        $data['layout'] = 'mobi';
        $data['main_content'] = '/specials/index';
        $this->load->view('includes/template',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */