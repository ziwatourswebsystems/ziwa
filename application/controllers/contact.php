<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

    function __construct()
    {
       parent::__construct();
    }

	public function index()
	{
		$data['layout'] = 'mobi';
        $data['main_content'] = '/contact/index';
        $this->load->view('includes/template',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */