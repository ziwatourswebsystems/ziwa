<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tours extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
        $this->load->model('tour_model','tours');
    }

	public function index()
	{
        $data['tour_count'] = $this->tours->countTours();
        $data['tours'] = $this->tours->get_all_tours();
        $data['layout'] = 'mobi';
        $data['main_content'] = '/tours/index';
        $this->load->view('includes/template',$data);
	}

    public function exclusive()
    {
        $data['layout'] = 'mobi';
        $data['main_content'] = '/tours/exclusive';
        $this->load->view('includes/template',$data);
    }

    public function view_tour($tour_id = 1)
    {
        $data['tour'] = $this->tours->get_tour_details($tour_id);
        $data['layout'] = 'mobi';
        $data['main_content'] = '/tours/view_tour';
        $this->load->view('includes/template',$data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */