<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct()
    {
        session_start();
        parent::__construct();
        $this->load->model('home_model','home');
    }

	public function index()
	{
       // $data['testimonials'] = $this->home->get_all_testimonials();
        $data['specials'] = $this->home->get_all_specials();
        $data['tours'] = $this->home->get_all_tours();
        $data['exclusive_tours'] = $this->home->get_all_exclusives();
        $data['packages'] = $this->home->get_all_packages();
        $data['layout'] = 'mobi';
        $data['main_content'] = '/home/index';
        $this->load->view('includes/template',$data);
	}

    public function view_testimonials($main_id = 0)
    {
        if($main_id != 0)
        {
            $data['testimonials_main'] = $this->home->get_main_testimonials($main_id);
        }
        $data['testimonials'] = $this->home->get_all_testimonials();
        $data['main_id'] = $main_id;
        $data['layout'] = 'mobi';
        $data['main_content'] = '/home/view_testimonials';
        $this->load->view('includes/template',$data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */