<script src="<?php echo base_url(); ?>assets/js/contact.js"></script>
<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->

</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn bg-white clearfix">
            <!-- Breakcrumb -->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li>Enquiries</li>
                </ul>
            </section>
            <!-- End Breakcrumb -->
            <section class="contact-page">
                <div class="contact-maps">
                    <div id="contact-maps" data-map-zoom="16" data-map-latlng="-33.93, 18.460000000000036" data-map-content="Book Awesome"></div>
                </div>
                <div class="contact-cn">
                    <div id="enquire_msg" class="alert-box alert-success" style="display:none">
                        <h6 id="enquire_heading">Success</h6>
                        <span id="enquire_body">Your enquire has been sent.</span>
                    </div>
                    <h2>We are always in touch</h2>
                    <ul>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/images/icon-phone.png" alt="">
                            +27 21 837 1617
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/images/icon-phone.png" alt="">
                            +27 82 389 0895
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/images/icon-email.png" alt="">
                            <a href="">info@ziwatours.biz</a>
                        </li>
                        <li>
                            <img src="<?php echo base_url(); ?>assets/images/icon-email.png" alt="">
                            <a href="">ismail@ziwatours.biz</a>
                        </li>





                    </ul>
                    <div class="form-contact">
                        <form id="contact-form" action="#" method="post">

                            <div class="form-field">
                                Enquiry for <sup>*</sup>
                            </div>
                            <div class="form-field">
                                <select id="subject" name="subject" class="field-input">
                                    <option>General</option>
                                    <option>Tours</option>
                                    <option>Packages</option>
                                    <option>Booking</option>
                                </select>
                            </div>
                            <div class="form-field">
                                <label for="ref">Reference <sup>If applicable</sup></label>
                                <input type="text" name="ref" id="ref" class="field-input">
                            </div>
                            <div class="form-field">
                                <label for="name">Name <sup>*</sup></label>
                                <input type="text" name="name" id="name" class="field-input">
                            </div>
                            <div class="form-field">
                                <label for="email">Email <sup>*</sup></label>
                                <input type="text" name="email" id="email" class="field-input">
                            </div>
                            <div class="form-field form-field-area">
                                <label for="message">Message <sup>*</sup></label>
                                <textarea name="message" id="message" cols="30" rows="10" class="field-input"></textarea>
                            </div>
                            <div class="form-field text-center">
                                <button type="submit" id="submit-contact" class="awe-btn awe-btn-2 arrow-right arrow-white awe-btn-lager">Submit Enquiry</button>
                            </div>
                            <div id="contact-content">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- End Main -->