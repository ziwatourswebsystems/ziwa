<script src="<?php echo base_url(); ?>assets/js/tour.js"></script>

<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->

</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn tour-page bg-white clearfix">
            <div class="row">

                <!-- Cruise Right -->
                <div class="col-md-9 col-md-push-3">

                    <!-- Breakcrumb -->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="#"><i class="fa fa-home"></i></a></li>
                            <li>Tours</li>
                            <li>All</li>
                        </ul>
                        <div class="support float-right">
                            <small>Got a question?</small> +27 82 389 0895
                        </div>
                    </section>
                    <!-- End Breakcrumb -->

                    <section class="cruise-list">




                        <!-- Cruise Content -->
                        <div class="cruise-list-cn tour-list-cn">

                            <?php

                            //print_r($tours);

                            foreach($tours as $item) {
                                $classes = str_replace(" ", "_", $item['tour_tags']);
                                $classes = str_replace(",", " ", $classes);
                                ?>

                                <!-- Item -->
                                <div class="cruise-item <?php echo $classes; ?>">
                                    <figure class="cruise-img">
                                        <a href="/tours/view_tour">
                                            <?php
                                            if($item['tour_main_image'] != "")
                                            {
                                                ?>
                                                <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" >
                                                <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $item['tour_id']; ?>/main/<?php echo $item['tour_main_image']; ?>" alt="">
                                                </a>
                                            <?php
                                            }else {
                                                ?>
                                                <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>">
                                                <img src="<?php echo base_url(); ?>assets/images/tour/img-6.jpg" alt="">
                                                </a>
                                            <?php
                                            }
                                            ?>
                                        </a>
                                    </figure>
                                    <div class="cruise-text">
                                        <div class="cruise-name">
                                            <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>"><?php echo urldecode($item['tour_name']); ?></a>
                                        </div>
                                        <ul class="ship-port">
                                            <li>
                                                <span class="label">Featuring:</span>
                                                <?php echo urldecode(substr($item['tour_overview'],0,200).'...'); ?>
                                            </li>
                                        </ul>
                                        <div class="price-box">
                                            <span class="price">
                                                <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>"
                                                   class="awe-btn awe-btn-4 arrow-right awe-btn-small">Book</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                            <?php
                            }
                            ?>

                        </div>
                        <!-- End Cruise Content -->


                    </section>
                </div>
                <!-- End Cruise Right -->

                <!-- Sidebar Hotel -->
                <div class="col-md-3 col-md-pull-9">
                    <!-- Sidebar Content -->
                    <div class="sidebar-cn">
                        <!-- Search Result -->
                        <div class="search-result">
                            <p>
                                We have <br>
                                <ins><?php echo $tour_count; ?></ins> <span>tours available</span>
                            </p>
                        </div>
                        <!-- End Search Result -->

                        <!-- Hotel Location -->
                        <div class="widget-sidebar facilities-sidebar">
                            <h4 class="title-sidebar">Tour Types</h4>
                            <ul class="widget-ul">
                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Full_Day" id="facilities-1" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-1">Full Day</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Half_Day" id="facilities-2" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-2">Half Day</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Packaged" id="facilities-6" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-6">Packaged</label>
                                    </div>
                                </li>
                                <!--
                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Safari"  id="facilities-3" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-3">Safari</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Garden_Route" id="facilities-4" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-4">Garden Route</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Extended" id="facilities-5" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-5">Extended</label>
                                    </div>
                                </li>

                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Golf" id="facilities-7" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-7">Golf</label>
                                    </div>
                                </li>
                                <li>
                                    <div class="radio-checkbox">
                                        <input data-tid="Helicopter" id="facilities-8" type="checkbox" class="checkbox" checked />
                                        <label for="facilities-8">Helicopter</label>
                                    </div>
                                </li>
                                -->
                            </ul>
                        </div>
                        <!-- End Hotel facilities -->

                    </div>
                    <!-- End Sidebar Content -->
                </div>
                <!-- End Sidebar Hotel -->

            </div>
        </div>
    </div>
</div>
<!-- End Main -->