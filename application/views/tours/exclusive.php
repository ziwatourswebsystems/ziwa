<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->

</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn tour-page bg-white clearfix">
            <div class="row">

                <!-- Cruise Right -->
                <div class="col-md-9 col-md-push-3">

                    <!-- Breakcrumb -->
                    <section class="breakcrumb-sc">
                        <ul class="breadcrumb arrow">
                            <li><a href="#"><i class="fa fa-home"></i></a></li>
                            <li>Ziwa</li>
                            <li>Tours</li>
                            <li>Exclusive</li>
                        </ul>
                    </section>
                    <!-- End Breakcrumb -->

                    <section class="cruise-list">


                        <!-- Cruise Content -->
                        <div class="cruise-list-cn tour-list-cn">
                            <!-- Item -->
                            <div class="cruise-item">
                                <figure class="cruise-img">
                                    <a href="/tours/view_tour">
                                        <img src="<?php echo base_url(); ?>assets/images/tour/img-6.jpg" alt="">
                                    </a>
                                </figure>
                                <div class="cruise-text">
                                    <div class="cruise-name">
                                        <a href="/tours/view_tour">Helicopter Tours</a>
                                    </div>
                                    <ul class="ship-port">
                                        <li>
                                            <span class="label">Featuring:</span>
                                            V&A Waterfront, Table Mountain and much more.
                                        </li>
                                    </ul>
                                    <div class="price-box">
                                            <span class="price">
                                                <a href="/tours/view_tour" class="awe-btn awe-btn-4 arrow-right awe-btn-small">Details</a>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            <!-- End Item -->

                            <!-- Item -->
                            <div class="cruise-item">
                                <figure class="cruise-img">
                                    <a href="/tours/view_tour">
                                        <img src="<?php echo base_url(); ?>assets/images/tour/img-7.jpg" alt="">
                                    </a>
                                </figure>
                                <div class="cruise-text">
                                    <div class="cruise-name">
                                        <a href="/tours/view_tour">Golf Tours</a>
                                    </div>
                                    <ul class="ship-port">
                                        <li>
                                            <span class="label">Featuring:</span>
                                            Steenberg,Royal Cape and much more.
                                        </li>
                                    </ul>
                                    <div class="price-box">
                                           <span class="price">
                                                <a href="/tours/view_tour" class="awe-btn awe-btn-4 arrow-right awe-btn-small">Details</a>
                                           </span>
                                    </div>
                                </div>
                            </div>
                            <!-- End Item -->



                        </div>
                        <!-- End Cruise Content -->

                    </section>
                </div>
                <!-- End Cruise Right -->

                <!-- Sidebar Hotel -->
                <div class="col-md-3 col-md-pull-9">
                    <!-- Sidebar Content -->
                    <div class="sidebar-cn">
                        <!-- Search Result -->
                        <div class="search-result">
                            <p>
                                We have <br>
                                <ins>2</ins> <span>Exclusive Tours available</span>
                            </p>
                        </div>
                        <!-- End Search Result -->



                    </div>
                    <!-- End Sidebar Content -->
                </div>
                <!-- End Sidebar Hotel -->

            </div>
        </div>
    </div>
</div>
<!-- End Main -->