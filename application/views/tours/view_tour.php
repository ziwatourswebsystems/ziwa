<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->


</section>
<!--End Banner-->

<!-- Main -->
<div class="main main-dt">
    <div class="container">
        <div class="main-cn detail-page bg-white clearfix">

            <!-- Breakcrumb -->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="#" title="">Cape</a></li>
                    <li><a href="#" title="">Tours</a></li>
                    <li><?php echo urldecode($tour->tour_name); ?></li>
                </ul>
                <div class="support float-right">
                    <small>Got a question?</small> +27 82 389 0895
                </div>
            </section>
            <!-- End Breakcrumb -->

            <!-- Header Detail -->
            <section class="head-detail">
                <div class="head-dt-cn">
                    <div class="row">
                        <div class="col-sm-7">
                            <h1><?php echo urldecode($tour->tour_name); ?></h1>
                        </div>
                        <div class="col-sm-5 text-right">
                            <p class="price-book">
                                <a href="/booking/index/<?php echo $tour->tour_id; ?>" title="" class="awe-btn awe-btn-1 awe-btn-lager">Book Now</a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header Detail -->

            <!-- Detail Slide -->
            <section class="detail-slider">
                <!-- Lager Image -->
                <div class="slide-room-lg">
                    <div id="slide-room-lg">
                        <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $tour->tour_id; ?>/main/<?php echo $tour->tour_main_image; ?>" alt="">

                        <?php
                        $tour_images = explode(",",$tour->tour_images);
                        foreach($tour_images as $tour_image) {
                            ?>
                            <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $tour->tour_id; ?>/<?php echo $tour_image; ?>" alt="">
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <!-- End Lager Image -->
                <!-- Thumnail Image -->
                <div class="slide-room-sm">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div id="slide-room-sm">

                                <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $tour->tour_id; ?>/main/<?php echo $tour->tour_main_image; ?>" alt="">


                                <?php
                                $tour_images = explode(",",$tour->tour_images);
                                foreach($tour_images as $tour_image) {
                                ?>
                                    <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $tour->tour_id; ?>/<?php echo $tour_image; ?>" alt="">
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Thumnail Image -->
            </section>
            <!-- End Detail Slide -->

            <!-- Tour Overview -->
            <section class="tour-overview detail-cn" id="tour-overview">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2>overview</h2>
                            <hr class="hr">
                            <a href="#about-tour" title="">Highlights</a>
                            <?php
                                if($tour->tour_map != null) {
                            ?>
                                    <a href="#area-map" title="">Area Map</a>
                            <?php
                                }
                            ?>
                        </div>
                    </div>

                    <!-- Tour Overview Content -->
                    <div class="col-lg-9 tour-overview-cn">

                        <!-- Description -->
                        <div class="tour-description">
                            <h2 class="title-detail">
                                Overview
                            </h2>
                            <?php
                            $string_about = preg_replace('/\n{2,}/', "</p><p>", trim($tour->tour_overview));
                            $string_about = preg_replace('/\n/', '</p><p>', $tour->tour_overview);
                            $string_about = "<p>".urldecode($string_about)."</p>";
                            ?>

                            <div class="tour-detail-text">
                                <?php echo $string_about; ?>
                            </div>
                        </div>
                        <!-- End Description -->
                        <br />
                        <div class="cruise-overview-item">
                            <h2 class="title-detail">Tour Options</h2>
                            <div class="text">
                                <ul class="ship-highlight-list">

                                    <?php
                                    $tour_tags = explode(",",$tour->tour_tags);
                                    foreach($tour_tags as $tag_item) {
                                    ?>
                                        <li><?php echo $tag_item; ?></li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </div>



                        </div>
                    </div>
                    <!-- End Tour Overview Content -->

                </div>
            </section>
            <!-- End Tour Overview -->

            <!-- Optional Activities -->
            <section class="about-tour detail-cn" id="about-tour">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2>Highlights</h2>
                            <hr class="hr">
                            <a href="#tour-overview" title="">Overview</a>
                            <?php
                            if($tour->tour_map != null) {
                            ?>
                            <a href="#area-map" title="">Area Map</a>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                    $string_about = preg_replace('/\n{2,}/', "</p><p>", trim($tour->tour_about));
                    $string_about = preg_replace('/\n/', '</p><p>', $tour->tour_about);
                    $string_about = "<p>".urldecode($string_about)."</p>";
                    ?>
                    <div class="col-lg-9 about-tour-cn">
                        <div class="policies-item">
                            <h2 class="title-detail">Highlights</h2>
                            <div class="tour-detail-text">
                            <?php echo $string_about; ?>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <!-- End Optional Activities -->


            <?php
            if($tour->tour_map != null) {
            ?>
            <!-- Meals -->
            <section class="area-map detail-cn" id="area-map">
                <div class="row">
                    <div class="col-lg-3 detail-sidebar">
                        <div class="scroll-heading">
                            <h2>Area Map</h2>
                            <hr class="hr">
                            <a href="#tour-overview" title="">Overview</a>
                            <a href="#about-tour" title="">Highlights</a>
                        </div>
                    </div>
                    <div class="col-lg-9 area-map-cn">
                        <div class="policies-item">
                            <h3>Area Map</h3>
                            <br />
                            <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $tour->tour_id; ?>/map/<?php echo $tour->tour_map; ?>" alt="">
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Meals-->
            <?php
            }
            ?>

            <section class="detail-footer tour-detail-footer detail-cn">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-9 detail-footer-cn text-right">
                        <p class="price-book">
                            <a href="/booking/index/<?php echo $tour->tour_id; ?>" title="" class="awe-btn awe-btn-1 awe-btn-lager">Book Now</a>
                        </p>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- End Main -->
