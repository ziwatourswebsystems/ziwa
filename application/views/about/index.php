<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-7"></div>
    <!--End Background-->

</section>
<!--End Banner-->
<style>
    td {
        padding: 15px;
    }
</style>
<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn about-page bg-white clearfix">

            <!-- Breakcrumb -->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li>About us</li>

                </ul>
                <div class="support float-right">
                    <small>Got a question?</small> +27 82 389 0895
                </div>
            </section>
            <!-- End Breakcrumb -->
            <!-- About -->
            <section class="about-cn clearfix">

                <div class="about-text">
                    <h1>About Ziwa Tours</h1>
                    <div class="about-description">
                        <p>Ziwa Tours was founded by Ismail Ziwa and is a fully registered company operating since 2003. Ismail is a registered tour guide, and also a member of Cape Town tourism council. Ziwa Tours pride themselves at what they do and always strive to deliver top class service.</p>
                        <p>Safety and security is at the top of our agenda and we always strive for the best customer satisfaction. Our customers are of utmost importance to us, that is why Ziwa Tours strive for customer satisfaction.</p>
                        <p>Our staff are fully trained and will always try to give 100% commitment when it comes to the need of the customer. Tourists requiring safe and reliable transport and competitive rates are assured of the highest level of service at all times. We have qualified tour - guides that will make the journey a memorable one. French speakers guides, Portuguese and English.</p>
                        <p>Private chauffeur services is also offered. We know how important clients are to our company that’s why we will go the extra mile to give the best service.</p>
                    </div>
                </div>
            </section>
            <!-- End About -->

            <!-- Team -->
            <section class="team">
                <div class="team-head">
                    <h2>Our Service</h2>
                    <p>
                        Our service is readily available and we would be happy to discuss your transport needs and offer a cost effective solution. Our vehicles are latest models and are serviced regularly and are equipped with A/C and seat belts. All vehicles are standard with a taxi meter and our tariffs are the lowest in Cape Town. All vehicles are insured for passenger liability and we have satellite tracking installed and know at any given time where our vehicles are, thereby monitoring the safety of our passengers.
                    </p>
                    <h2>Our team</h2>
                    <p>
                        Our staff are fully trained and will always try to give 100% commitment when it comes to the need of the customer.
                    </p>
                </div>
                <div class="team-group row">
                    <!-- Team Item -->
                    <div class="team-item col-xs-6 col-md-3">
                        <figure>
                            <img width="255" height="162" src="<?php echo base_url(); ?>assets/profiles/ismail_ziwa.jpg" alt="">
                        </figure>
                        <h3>Ismail Ziwa</h3>
                        <span>Founder</span>
                    </div>
                    <!-- End Team Item -->
                    <!-- Team Item -->
                    <div class="team-item col-xs-6 col-md-3">
                        <figure>
                            <img src="<?php echo base_url(); ?>assets/images/ziwatours.png" alt="">
                        </figure>
                        <h3>JHON DOE</h3>
                        <span>Tour Operator</span>
                    </div>
                    <!-- End Team Item -->

                </div>
            </section>
            <!-- End Team -->


        </div>
    </div>
</div>