<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-4"></div>
    <!--End Background-->

</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn about-page bg-white clearfix">

            <!-- Breakcrumb -->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li>About South Africa</li>
                </ul>
            </section>
            <!-- End Breakcrumb -->
            <!-- About -->
            <section class="about-cn clearfix">
                <div class="about-text">
                    <h1>About South Africa</h1>
                    <div class="about-description">
                        <div class="team-item col-xs-6 col-md-3">
                        <img src="<?php echo base_url(); ?>assets/images/sa_map.gif" />
                        </div>
                        <p>
                            GAUTENG, NORTH WEST, NORTHERN PROVINCE, MPUMALANGA, KWA-ZULU, NATAL, WESTERN CAPE, EASTERN CAPE and the FREE STATE. The country lies in the southern temperate zone, most on plateau’s above 1200m-4000 feet. Home of about 47 million population.
                        </p>
                        <p>
                            SESOTHO, TSWANA, PEDI, ZULU, XHOSA, VENDA, TSONGA, SWATI, NDEBELE, ENGLISH and AFRIKAANS. South Africa is the world biggest producer of Gold, Platinum, Manganese, Alumino-Silicates, Diamonds, etc. The big hole in Kimberley where diamonds were mined, is the largest hand dug mine in the world. The gold mine, the Western Deep Level at 3777 meters is the deepest in the world.                   </p>
                        <br />
                        <p>
                            South Africa boasts a variety of indigenous, free-roaming wildlife and has many national parks and game reserves.

                            Whether is the Big 5, or looking for birds and tortoise, this is the place to be. South Africa is the only country in the world with an entire floral kingdom within its boundaries.
                        </p>

                        <table width="100%">
                            <tr>
                                <td width="50%">
                                    <b>The country has three Capitals: i.e.</b>
                                </td>
                                <td width="50%">
                                    <b>The country's main harbours are :</b>
                                </td>

                            </tr>
                        <tr>
                          <td width="50%">
                                <ul>
                                    <li>Durban</li>
                                    <li>Cape Town</li>
                                    <li>Port Elizabeth</li>
                                    <li>Richards Bay</li>
                                    <li>Saldanha Bay</li>
                                </ul>
                           </td>
                            <td width="50%">
                                <ul>
                                    <li>Pretoria - The administrative</li>
                                    <li>Bloemfontein - The judicial</li>
                                    <li>Cape Town - The legislative</li>
                                    <li>Parliament sits in the Cape</li>
                                </ul>
                            </td>

                        </tr>
                        </table>

                    </div>
                </div>
            </section>
            <!-- End About -->
            <!-- Team -->
            <section class="team">
                <div class="team-head">
                    <p>
                        The East side is where the land is rich in grassland, savannah and forest.

                        The West side is where the greater portion is dry thornveld and semi-desert.

                        Temperature in winter time is about 20 degrees Celsius (60 Fahrenheit) and summer time is about 30 degrees Celsius (100 degrees Fahrenheit). Average rainfall is about 500 mm annually and sunshine 7-9 hours daily. Most of the South Africans live in the Eastern region of the country, where higher rainfall, better soil and rich minerals often provide more job opportunities. Friendly people abound in South Africa, most people live a very sociable lifestyle. They love eating, drinking, chatting and welcoming visitors. You will be spoilt by their great food, good service and lots of smiles. In a recent business survey, South Africa was listed as one of the cheapest yet it compares favourably with the most expensive.

                        Trade: Exports-Gold, Diamonds, Metals, Food, Tropical Products.

                        Imports-Machines, Transport Equipment, Manufactures goods, Chemicals and Oil

                        Currency: Rand(R)
                    </p>
                    <br />
                    <h2>Key facts about S.A</h2>
                    <p>
                        <ul>
                            <li>Area; 1,219,090sqkms</li>
                            <li>Coastline; approx. 3000kms long.</li>
                            <li>Biggest cities; Johannesburg, cape town and Durban</li>
                            <li>Population 54 million people.</li>
                            <li>Main languages;  IsiZulu, IsiXhosa Afrikaans,English</li>
                            <li>Best time to visit: Sept/oct(spring) , April/may(autumn)</li>
                            <li>Main economic  sectors; mining, manufacturing, financial services, tourism.</li>
                            <li>Climate; western cape got Mediterranean climate.</li>
                            <li>Currency; Rands (ZAR)</li>
                            <li>Time zone; GMT+2</li>
                            <li>International dialing code;+27</li>
                        </ul>
                    </p>
                </div>



        </div>
    </div>
</div>