<!-- JavaScript Library -->
<script src="<?php echo base_url(); ?>assets//js/library/modernizr-2.6.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets//js/library/jquery-1.10.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets//js/library/jquery-ui-1.10.3.custom.min.js"></script>
<!--[if lte IE 8]>
<script src="<?php echo base_url(); ?>assets//js/library/excanvas.min.js"></script>
<![endif]-->
<!-- Main APP -->
<script src="<?php echo base_url(); ?>assets//js/app.js"></script>
<!-- Demo -->
<script src="<?php echo base_url(); ?>assets//js/demo.js"></script>
<!-- JavaScript Modules -->
<script src="<?php echo base_url(); ?>assets//js/module/module.min.js"></script>
<!-- Third Party Plugins -->
<script src="<?php echo base_url(); ?>assets//plugins/flot/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/flot/jquery.flot.selection.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/flot/jquery.flot.time.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/flot/jquery.flot.crosshair.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/google-code-prettify/prettify.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/ChartJS/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets//plugins/easy-pie-chart/jquery.easy-pie-chart.js"></script>
<!-- Page Script -->
<script>
	App.setOptions({
		spa: false
	});
</script>
</body>
</html>