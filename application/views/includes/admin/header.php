<!DOCTYPE html>
<html>
<head>
	<title>Uusi Jobs</title>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=0" />
	<!-- CSS Library -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/library/normalize.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/library/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/library/jquery-ui-1.10.3.custom.min.css">
	<!-- Theme -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/default/layout.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/default/core_colour.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme/default/colour5.css" class="theme-colour">
	<!-- Third Party Plugins -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/google-code-prettify/prettify.css">
	<link rel="stylesheet" href=".<?php echo base_url(); ?>assets/lugins/easy-pie-chart/jquery.easy-pie-chart.css">
	<!-- Demo -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demo.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
    <script src="<?php echo base_url(); ?>assets/js/library/jquery.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/parsley.min.js"></script>
</head>
<body>
<!-- Portrait Detector -->
<div id="portrait_mode_detector"></div>
<!-- End -->
<div id="layout-container">
	<!-- Menu -->
	<div id="nav">
		<!-- Menu -->
        <?php  $this->view('blocks/admin_mainnav'); ?>
		<!-- End -->
	</div>
	<!-- End -->
	<!-- Page Container -->
	<div id="main">
		<div class="page-title">
			<div class="menu-switch"><i class="icon-reorder"></i></div>
			<img src="/assets/images/uusi-logo.png" alt="uusi logo" style="width:7em;height:4em;" >
		</div>