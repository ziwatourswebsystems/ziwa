<nav class="navigation nav-c" id="navigation" data-menu-type="1200">
    <div class="nav-inner">
        <a href="#" class="bars-close" id="bars-close">Close</a>
        <div class="tb">
            <div class="tb-cell">
                <ul class="menu-list text-uppercase">
                    <li class="current-menu-parent home topnavmnu">
                        <a href="/home" title="">Home</a>
                    </li>
                    <li class="about topnavmnu">
                        <a href="/about" title="">About</a>
                        <!--
                        <ul class="sub-menu">
                            <li>
                                <a href="/about/aboutsa">About South Africa</a>
                            </li>
                        </ul>
                        -->
                    </li>
                    <li class="gallery topnavmnu">
                        <a href="/gallery" title="">Gallery</a>
                    </li>
                    <li class="services topnavmnu">
                        <a href="/services" title="">Services</a>
                    </li>
                    <li class="tours topnavmnu">
                        <a href="/tours" title="">Tours</a>

                    </li>
                    <li class="blog topnavmnu">
                        <a href="/blog" title="">Blog</a>

                    </li>
                    <li class="specials topnavmnu">
                        <a href="/specials" title="">Specials</a>
                    </li>
                    <li class="enquire topnavmnu">
                        <a href="/enquire" title="">Enquire</a>
                    </li>
                    <li class="members topnavmnu">
                        <a href="http://members.ziwatours.biz" title="">Members</a>
                    </li>
                    <!--

                    <li>
                        <a href="#">About</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#" title="">Blog</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="blog.html" title="">Blog</a>
                                    </li>
                                    <li>
                                        <a href="blog-detail.html">Blog Detail</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="about.html" title="">About</a></li>
                            <li><a href="contact.html" title="">Contact</a></li>
                            <li><a href="payment.html" title="">Payment</a></li>
                            <li><a href="element.html" title="">Element</a></li>
                            <li><a href="404.html" title="">404</a></li>
                            <li><a href="comingsoon.html" title="">Comingsoon</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="">Hotel</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#" title="">Hotel</a>
                            </li>
                            <li>
                                <a href="#">Hotel List 1</a>
                            </li>
                            <li>
                                <a href="#">Hotel List 2</a>
                            </li>
                            <li>
                                <a href="#">Hotel Map</a>
                            </li>
                            <li>
                                <a href="#">Hotel Detail</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="">Flights</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#" title="">Flights</a>
                            </li>
                            <li>
                                <a href="#">Flight List</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="">Car</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#" title="">Car</a>
                            </li>
                            <li>
                                <a href="#">Cart List</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="">Package</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#" title="">Package Deals</a>
                            </li>
                            <li>
                                <a href="#">Package Deals List</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="">Cruises</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#" title="">Cruises</a>
                            </li>
                            <li>
                                <a href="#">Cruise List</a>
                            </li>
                            <li>
                                <a href="#">Cruise Detail</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" title="">Tours</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#" title="">Tours</a>
                            </li>
                            <li>
                                <a href="#">Tour List</a>
                            </li>
                            <li>
                                <a href="#">Tour Detail</a>
                            </li>
                        </ul>
                    </li>
                    -->
                </ul>
            </div>
        </div>
    </div>
</nav>