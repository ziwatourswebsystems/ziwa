<?php

if ($layout === 'admin')
{
	$path_header = 'includes/admin/header';
	$path_footer = 'includes/admin/footer';
}
else
{
	$path_header = 'includes/mobi/header';
	$path_footer = 'includes/mobi/footer';
}
$this->load->view($path_header);
$this->load->view($main_content);
$this->load->view($path_footer);