<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <!-- Logo -->
            <div class="col-md-4">
                <div class="logo-foter">

                </div>
            </div>
            <!-- End Logo -->
            <!-- Navigation Footer -->
            <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="ul-ft">

                </div>
            </div>
            <!-- End Navigation Footer -->
            <!-- Navigation Footer -->
            <div class="col-xs-6 col-sm-3 col-md-2">
                <div class="ul-ft">

                </div>
            </div>
            <!-- End Navigation Footer -->
            <!-- Footer Currency, Language -->
            <div class="col-sm-6 col-md-4">


                <!-- Currency
                <div class="currency-lang-bottom dropdown-cn float-left">
                    <div class="dropdown-head">
                        <span class="angle-down"><i class="fa fa-angle-down"></i></span>
                    </div>
                    <div class="dropdown-body">
                        <ul>
                            <li class="current"><a href="#" title="">US</a></li>
                            <li><a href="#" title="">ARS</a></li>
                            <li><a href="#" title="">ADU</a></li>
                            <li><a href="#" title="">CAD</a></li>
                            <li><a href="#" title="">CHF</a></li>
                            <li><a href="#" title="">CNY</a></li>
                            <li><a href="#" title="">CZK</a></li>
                        </ul>
                    </div>
                </div>
                -->
                <!-- End Currency -->
                <!--CopyRight-->
                <p class="copyright">
                    © <?php  echo date('Y'); ?> Ziwa Tours All rights reserved.
                </p>
                <!--CopyRight-->
            </div>
            <!-- End Footer Currency, Language -->
        </div>
    </div>
</footer>
<div id="google_translate_element"></div><script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
    }
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63325821-1', 'auto');
    ga('send', 'pageview');

</script>

<!-- Library JS -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/jquery.nicescroll.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/SmoothScroll.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/library/jquery.validate.min.js"></script>
<!-- End Library JS -->
<!-- Main Js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>
<!-- End Main Js -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>

<script type="text/javascript" data-cfasync="false">(function () { var done = false;var script = document.createElement('script');script.async = true;script.type = 'text/javascript';script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';document.getElementsByTagName('HEAD').item(0).appendChild(script);script.onreadystatechange = script.onload = function (e) {if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {var w = new PCWidget({ c: '4d2c86ee-e245-497a-9214-45ffc6eb5aa2', f: true });done = true;}};})();</script>
</body>
</html>