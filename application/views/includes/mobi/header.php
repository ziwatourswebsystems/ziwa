
<!DOCTYPE html>
<!--[if IE 7 ]> <html class="ie ie7"> <![endif]-->
<!--[if IE 8 ]> <html class="ie ie8"> <![endif]-->
<!--[if IE 9 ]> <html class="ie ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Ziwa Tours Home Cape Town Transport Service</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="keywords" content="ziwa tours, cape town, tours, airport transfers, 24 hour service ,guided tours,chauffeurs, transport for cape town, travel information cape town, ,cape town private transportation, cape town transportation, cape town sightseeing, city sightseeing, cape town tours,private drivers, tble mountain, camps bay, luxury private tours, tours services, boutique hotels cape town, private tours, safe tours, friendly tours, best tours, 24/7 tours , driver, cabs, cape town tourism, cape town transport, cape town services, hotels,airports, packages, ismail ziwa, cape">
    <meta name="description" content="Official Cape Tours Ziwa Tours, Transport, Services Cape Town">
    <meta name="google-translate-customization" content="de71b8ffd9bd48ad-5cadfbbb343912fc-g36fe5d3473f49eca-9">
    <!-- Font Google -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400%7COpen+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <!-- End Font Google -->
    <!-- Library CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/library/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/library/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/library/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/library/owl.carousel.css">
    <!-- End Library CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <!-- End Library CSS
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
    -->
    <script src="<?php echo base_url(); ?>assets/js/library/jquery-1.11.0.min.js" ></script>

    <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-image-gallery.min.css">

</head>
<body>
<!-- Preloader -->
<div id="preloader">
    <div class="tb-cell">
        <div id="page-loading">
            <div></div>
            <p>Loading</p>
        </div>
    </div>
</div>
<!-- Wrap -->
<div id="wrap">

    <!-- Header -->
    <header id="header" class="header">
        <div class="container" >
            <!-- Logo -->
            <div class="logo float-left">
                <img  src="<?php echo base_url(); ?>assets/images/ziwaologo.jpg" alt="Ziwa Cape Tours Logo Place Holder">
            </div>
            <!-- End Logo -->
            <!-- Bars -->
            <div class="bars" id="bars"></div>
            <!-- End Bars -->

            <!--Navigation-->
            <?php
            $this->load->view('includes/nav');
            ?>
            <!--End Navigation-->
        </div>
    </header>
    <!-- End Header -->