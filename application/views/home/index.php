<script src="<?php echo base_url(); ?>assets/js/booking.js"></script>

    <!--Banner-->
    <section class="banner">

        <!--Background-->
        <div class="bg-parallax bg-1"></div>
        <!--End Background-->

        <div class="container">

            <!-- Banner Content -->
            <div class="banner-cn">

                <!-- Tabs Cat Form -->
                <ul class="tabs-cat text-center row">
                    <li class="cate-item active col-xs-2">
                        <a data-toggle="tab" href="#form-car" title="">
                            <span>Private Chauffeur</span>
                            <img src="<?php echo base_url(); ?>assets/images/icon-car.png" alt="cars"></a>
                    </li>
                    <li class="cate-item col-xs-2">
                        <a data-toggle="tab" href="#form-package" title="">
                            <span>package deals</span>
                            <img src="<?php echo base_url(); ?>assets/images/icon-tour.png" alt="packages"></a>
                    </li>
                    <li class="cate-item col-xs-2">
                        <a data-toggle="tab" href="#form-tour" title="">
                            <span>TOURs</span>
                            <img src="<?php echo base_url(); ?>assets/images/icon-vacation.png" alt=""></a>
                    </li>
                    <li class="cate-item col-xs-2">
                        <a data-toggle="tab" href="#form-cruise" title="">
                            <span>Exclusive</span>
                            <img src="<?php echo base_url(); ?>assets/images/icon-cruise.png" alt=""></a>
                    </li>
                </ul>
                <!-- End Tabs Cat -->

                <!-- Tabs Content -->
                <div class="tab-content">

                    <!-- Search Car -->
                    <div class="form-cn form-car tab-pane active in" id="form-car">
                        <h2>Where would you like to go?</h2>
                        <form id="private_chauffer_frm" name="private_chauffer_frm" action="" method="POST">
                        <div class="form-search clearfix">
                            <div class="form-field field-from">
                                <label for="flight-from"><span>Name:</span> eg. John</label>
                                <input id="private_name" type="text" name="private_name" id="flight-from" class="field-input">
                            </div>
                            <div class="form-field field-to">
                                <label for="flight-to"><span>Email :</span> eg. john@smith.com</label>
                                <input id="private_email" name="private_email" type="text" class="field-input">
                            </div>
                            <div class="form-field field-date">
                                <input id="private_date" name="private_date"  type="text" class="field-input calendar-input" placeholder="Pick up date">
                            </div>
                            <div class="form-field" >
                                <a href="#" id="btn_private_driver_booking" class="awe-btn awe-btn-1 awe-btn-medium">Request Booking</a>
                            </div>
                        </div>
                        </form>

                        <div id="private_success_message" class="alert-box alert-success" style="display:none">
                            <h6>Booking Request Success  message</h6>
                            <span>Congratulations <b id="psm_name"></b> you have provisionally completed your booking request. Please check the email we sent to <b id="psm_email"></b> for booking request confirmation details and instructions.Thank you for choosing Ziwa Tours one of our frinedly consultant will be in touch shortly</span>
                        </div>

                        <div id="private_error_message" class="alert-box alert-error" style="display:none">
                            <h6>Booking Request Error  message</h6>
                            <span id="pem_msg">All fields are required. Please complete all fields.</span>
                        </div>

                    </div>
                    <!-- End Search Car -->



                    <!-- Search Package -->
                    <div class="form-cn form-package tab-pane" id="form-package">
                        <h2>Choose your Package !</h2>
                        <br />
                        <form id="package_frm" name="package_frm" action="" method="POST">
                         <div class="form-search clearfix">
                            <div class="form-field field-from">
                                <label for="flight-from"><span>Name:</span> eg. John</label>
                                <input type="text"  id="package_name" name="package_name" class="field-input">
                            </div>
                            <div class="form-field field-to">
                                <label for="flight-to"><span>Email :</span> eg. john@smith.com</label>
                                <input type="text" id="package_email" name="package_email" class="field-input">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" id="package_date" name="package_date"  class="field-input calendar-input" placeholder="Pick up date">
                            </div>
                            <div class="form-field field-select field-country">
                                <div class="select">
                                    <span>Package</span>
                                    <select id="package_selected" name="package_selected" >
                                        <option value="0" >Please Select Package</option>
                                        <?php
                                        foreach($packages as $item)
                                        {
                                        ?>
                                        <option value="<?php echo $item['service_id']; ?>" ><?php echo $item['service_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-field" >
                                <a href="#" id="btn_book_package" class="awe-btn awe-btn-1 awe-btn-medium">Request Booking</a>
                            </div>
                             </form>
                        </div>

                        <div id="package_success_message" class="alert-box alert-success" style="display:none">
                            <h6>Booking Request Success  message</h6>
                            <span>Congratulations <b id="pksm_name"></b> you have provisionally completed your booking request. Please check the email we sent to <b id="pksm_email"></b> for booking request confirmation details and instructions.Thank you for choosing Ziwa Tours one of our frinedly consultant will be in touch shortly</span>
                        </div>

                        <div id="package_error_message" class="alert-box alert-error" style="display:none">
                            <h6>Booking Request Error  message</h6>
                            <span id="pkem_msg">All fields are required. Please complete all fields.</span>
                        </div>

                    </div>
                    <!-- End Search Package -->

                    <!-- Search Tour-->
                    <div class="form-cn form-tour tab-pane" id="form-tour">
                        <h2>Choose your Tour !</h2>

                        <div class="form-search clearfix">
                            <div class="form-field field-select field-country">
                                <div class="select">
                                    <span>Tours</span>

                                    <select id="sel_tours">
                                        <?php
                                        foreach($tours as $item) {
                                        ?>
                                            <option value="<?php echo $item['tour_id']; ?>"><?php echo $item['tour_name']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-field" >
                                <a id="lnk_view_tour_details" href="#" class="awe-btn awe-btn-1 awe-btn-medium">View Tour Details</a>
                            </div>
                            <div class="form-field" >
                                <a href="/tours" class="awe-btn awe-btn-3 arrow-right awe-btn-medium">Browse All Tours</a>
                            </div>

                        </div>
                    </div>
                    <!-- End Search Tour -->

                    <!-- Search Cruise-->
                    <div class="form-cn form-package tab-pane" id="form-cruise">
                        <h2>Choose your Exclusive !</h2>
                        <br />
                        <form id="exclusive_frm" name="exclusive_frm" action="" method="POST">
                            <div class="form-search clearfix">
                            <div class="form-field field-from">
                                <label for="flight-from"><span>Name:</span> eg. John</label>
                                <input type="text" name="exclusive_name" id="exclusive_name" class="field-input">
                            </div>
                            <div class="form-field field-to">
                                <label for="flight-to"><span>Email :</span> eg. john@smith.com</label>
                                <input type="text" id="exclusive_email" name="exclusive_email" class="field-input">
                            </div>
                            <div class="form-field field-date">
                                <input type="text" id="exclusive_date" name="exclusive_date" class="field-input calendar-input" placeholder="Pick up date">
                            </div>
                            <div class="form-field field-select field-country">
                            <div class="select">
                                <span>Exclusive Tours</span>
                                <select id="exclusive_select" name="exclusive_select" >
                                    <option value="0">Please Select Exclusive</option>
                                    <?php
                                    foreach($exclusive_tours as $item) {
                                        ?>
                                        <option value="<?php echo $item['tour_id']; ?>"><?php echo $item['tour_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            </div>
                            <div class="form-field" >
                                <a href="#" id="btn_exclusive_booking_request" class="awe-btn awe-btn-1 awe-btn-medium">Request Booking</a>
                            </div>
                           </form>
                        </div>

                        <div id="exclusive_success_message" class="alert-box alert-success" style="display:none">
                            <h6>Booking Request Success  message</h6>
                            <span>Congratulations <b id="xsm_name"></b> you have provisionally completed your booking request. Please check the email we sent to <b id="xsm_email"></b> for booking request confirmation details and instructions.Thank you for choosing Ziwa Tours one of our frinedly consultant will be in touch shortly</span>
                        </div>

                        <div id="exclusive_error_message" class="alert-box alert-error" style="display:none">
                            <h6>Booking Request Error  message</h6>
                            <span id="xem_msg">All fields are required. Please complete all fields.</span>
                        </div>

                    </div>
                    <!-- End Search Cruise -->



                </div>
                <!-- End Tabs Content -->

            </div>
            <!-- End Banner Content -->

        </div>

    </section>
    <!--End Banner-->


<div>
    <div class="row cs-sb-cn">




    <!-- Sales -->
    <section class="sales">
        <!-- Title -->
        <div class="title-wrap">
            <div class="container">
                <div class="travel-title float-left">
                    <h2>Hot Tours & Services:</h2>

                </div>
                <div class="support float-right">
                    <small>Got a question?</small> +27 82 389 0895
                </div>
                <div class="follow-us" style="float:right">
                    <h4>Connect with us</h4>
                    <div class="follow-group">
                        <a href="https://www.facebook.com/pages/Ziwascape-touring/133572513517134" title=""><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/ZiwaTours" title=""><i class="fa fa-twitter"></i></a>
                        <a href="http://www.tripadvisor.co.za/Attraction_Review-g312659-d6124159-Reviews-Ziwas_Cape_Touring_Day_Tours-Cape_Town_Central_Western_Cape.html" ><img src="/assets/images/icon-tripadvitsor.png" alt="trip advisor" /></a>
                    </div>
                </div>

            </div>

        </div>
        <!-- End Title -->
        <!-- Hot Sales Content -->
        <div class="container">

            <div class="sales-cn">

                <div class="row">

                    <?php

                    foreach($specials as $item) {
                        ?>
                        <!-- HostSales Item -->
                        <div class="col-xs-6 col-md-3">
                            <div class="sales-item">
                                <figure class="home-sales-imgd">
                                    <a href="" title="">
                                        <?php
                                        if($item['tour_main_image'] != "")
                                        {
                                        ?>
                                            <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" >
                                            <img height="292px" width="292px" src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $item['tour_id']; ?>/main/<?php echo $item['tour_main_image']; ?>" alt="Ziwa Tour Image">
                                            </a>
                                        <?php
                                        }else {
                                        ?>
                                            <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" >
                                            <img src="<?php echo base_url(); ?>assets/images/ziwatours.png" alt="Ziwa Tour Image">
                                            </a>
                                        <?php
                                        }
                                        ?>
                                    </a>

                                </figure>
                                <div class="home-sales-text">
                                    <div class="home-sales-name-places">
                                        <div class="home-sales-name">
                                            <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" title=""><?php echo urldecode($item['tour_name']); ?></a>
                                        </div>
                                        <div class="home-sales-places">
                                            <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" title="">  <?php echo urldecode(substr($item['tour_overview'],0,40).'...'); ?></a>
                                        </div>
                                    </div>
                                    <hr class="hr">
                                    <div class="price-box">
                                        <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" class="awe-btn awe-btn-4 arrow-right awe-btn-small">View
                                            Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End HostSales Item -->
                    <?php
                    }
                    ?>

                </div>
            </div>
        </div>
        <!-- End Hot Sales Content -->
    </section>
    <!-- End Sales -->


    <!-- Confidence and Subscribe  -->
    <section class="confidence-subscribe">
        <!-- Background -->
        <div class="bg-parallax bg-4"></div>
        <!-- End Background -->
        <div class="container" style="background-color:white">
            <div class="row cs-sb-cn">

                <!-- Confidence -->
                <div class="col-md-6">
                    <div class="confidence" style="background-color:white">
                        <h3>We are members of</h3>
                        <table>
                          <tr >
                              <td style="padding:10px;"> <a href="http://www.capetown.travel/"><img  width="150px" height="150px"  src="<?php echo base_url(); ?>assets/partners/capetowntourism.png" /></a></td>
                              <td style="padding:10px;"> <a href="http://www.satsa.com/" ><img width="150px" height="150px" src="<?php echo base_url(); ?>assets/partners/satsa.jpg" /></a></td>
                          </tr>
                        </table>
                    </div>
                </div>
                <!-- End Confidence -->
                <!-- Subscribe -->
                <div class="col-md-6">
                    <div class="subscribe">
                        <h3>Subscribe to our newsletter</h3>

                        <div id="subscriber_success_message" class="alert-box" style="display:none">
                            <h6 id="subscribe_heading">Subscriber Success Message</h6>
                            <span id="subscribe_msg">Congratulations <b id="pksm_name"></b> you have provisionally completed your booking request. Please check the email we sent to <b id="pksm_email"></b> for booking request confirmation details and instructions.Thank you for choosing Ziwa Tours one of our frinedly consultant will be in touch shortly</span>
                        </div>

                        <p>Enter your email address and we’ll send you our regular promotional emails, packed with special offers, great deals, and huge discounts</p>
                        <!-- Subscribe Form -->
                        <div class="subscribe-form">
                            <form action="#" method="get">
                                <input id="email_subscriber_fld" type="text" name="" value="" placeholder="Your email" class="subscribe-input">
                                <button id="btn_subscribe_add" type="submit" class="awe-btn awe-btn-5 arrow-right text-uppercase awe-btn-lager">subcrible</button>
                            </form>
                        </div>
                        <!-- End Subscribe Form -->
                        <!-- Follow us -->
                        <div class="follow-us">
                            <h4>Follow us</h4>
                            <div class="follow-group">
                                <a href="https://www.facebook.com/pages/Ziwascape-touring/133572513517134" title=""><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/ZiwaTours" title=""><i class="fa fa-twitter"></i></a>
                                <a href="http://www.tripadvisor.co.za/Attraction_Review-g312659-d6124159-Reviews-Ziwas_Cape_Touring_Day_Tours-Cape_Town_Central_Western_Cape.html" ><img src="/assets/images/icon-tripadvitsor.png" alt="trip advisor" /></a>
                            </div>
                        </div>
                        <!-- Follow us -->
                    </div>
                </div>
                <!-- End Subscribe -->

            </div>
        </div>
    </section>
    <!-- End Confidence and Subscribe  -->

