<script src="<?php echo base_url(); ?>assets/js/booking.js"></script>



    <!--Banner-->
    <section class="banner">

        <!--Background-->
        <div class="bg-parallax bg-1"></div>
        <!--End Background-->

        <div class="container">



        </div>

    </section>
    <!--End Banner-->





    <!-- Travel Magazine -->
    <section class="magazine">
        <!-- Title -->
        <div class="title-wrap">
            <div class="container">
                <div class="travel-title float-left">
                    <h2>Customer Testimonials</h2>
                </div>
                <a href="/home" title="" class="awe-btn awe-btn-5 arrow-right awe-btn-lager text-uppercase float-right">back home</a>
            </div>
        </div>
        <!-- End Title -->
        <!-- Magazine Content -->
        <div class="container">
            <div class="magazine-cn">
                <div class="row">
                    <!-- Magazine Descript -->
                    <div class="col-lg-6">
                        <div class="magazine-ds">
                            <div id="owl-magazine-ds">

                                <?php
                                    if(isset($testimonials_main)) {
                                ?>
                                <!-- Magazine Descript Item -->
                                <div class="magazine-item">
                                    <div class="magazine-header">
                                        <h2><?php echo $testimonials_main->t_headline; ?></h2>
                                        <ul>
                                            <li>by <a href="" title=""><?php echo ucwords ($testimonials_main->t_author); ?></a></li>
                                            <li><?php echo date('d.m.Y',strtotime($testimonials_main->t_date));?></li>
                                        </ul>
                                        <hr class="hr">
                                    </div>
                                    <?php
                                    $string = preg_replace('/\n{2,}/', "</p><p>", trim($testimonials_main->t_body));
                                    $string = preg_replace('/\n/', '</p><p>',$testimonials_main->t_body);
                                    $string = "<p>{$string}</p>";
                                    ?>
                                    <div class="magazine-body">
                                        <?php echo $string; ?>
                                    </div>
                                    <div class="magazine-footer clearfix">
                                    </div>
                                </div>
                                <!-- End Magazine Descript Item -->
                                 <?php
                                 }
                                //print_r($testimonials);
                                $home_count = 0;
                                foreach($testimonials as $item) {
                                    $home_count++;
                                    if($home_count <= 4) {
                                        if($main_id != $item['t_id']) {
                                            ?>
                                            <!-- Magazine Descript Item -->
                                            <div class="magazine-item">
                                                <div class="magazine-header">
                                                    <h2><?php echo $item['t_headline']; ?></h2>
                                                    <ul>
                                                        <li>by <a href=""
                                                                  title=""><?php echo ucwords($item['t_author']); ?></a>
                                                        </li>
                                                        <li><?php echo date('d.m.Y', strtotime($item['t_date'])); ?></li>
                                                    </ul>
                                                    <hr class="hr">
                                                </div>
                                                <?php
                                                $string = preg_replace('/\n{2,}/', "</p><p>", trim($item['t_body']));
                                                $string = preg_replace('/\n/', '</p><p>', $item['t_body']);
                                                $string = "<p>{$string}</p>";
                                                ?>
                                                <div class="magazine-body">
                                                    <?php echo $string; ?>
                                                </div>
                                                <div class="magazine-footer clearfix">
                                                    <div class="post-share magazine-share float-left">
                                                        <a data-t-id="<?php echo $item['t_id']; ?>"
                                                           href="https://www.facebook.com/sharer/sharer.php?u=http://ziwatours.biz/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                           title=""><i class="fa fa-facebook"></i></a>
                                                        <a data-t-id="<?php echo $item['t_id']; ?>"
                                                           href="https://twitter.com/home?status=http://ziwatours.biz/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                           title=""><i class="fa fa-twitter"></i></a>
                                                        <a data-t-id="<?php echo $item['t_id']; ?>"
                                                           href="https://plus.google.com/share?url=http://ziwatours.biz/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                           title=""><i class="fa fa-google-plus"></i></a>
                                                    </div>
                                                    <a href="/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                       title=""
                                                       class="awe-btn awe-btn-5 arrow-right awe-btn-lager text-uppercase float-right">view
                                                        more</a>
                                                </div>
                                            </div>
                                            <!-- End Magazine Descript Item -->
                                        <?php
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- End Magazine Descript -->
                    <!-- Magazine Thumnail -->
                    <div class="col-lg-6">
                        <div class="magazine-thum" id="magazine-thum">

                            <?php
                            if(isset($testimonials_main)) {
                                ?>
                                <!--Thumnail Item-->
                                <div class="thumnail-item active clearfix">
                                    <figure class="float-left">
                                        <?php
                                        if ($testimonials_main->t_imge == "") {
                                            ?>
                                            <img height="150" width="292"
                                                 src="<?php echo base_url(); ?>assets/images/magazine/img-2.jpg"
                                                 alt="">
                                        <?php } else { ?>
                                            <img height="150" width="292"
                                                 src="http://adminziwa/assets/uploads/testimonials/<?php echo $testimonials_main->t_imge; ?>"
                                                 alt="">
                                        <?php
                                        }
                                        ?>
                                    </figure>
                                    <div class="thumnail-text">
                                        <h4><?php echo $testimonials_main->t_headline; ?></h4>
                                        <span><?php echo date('d.m.Y',strtotime($testimonials_main->t_date));?></span>
                                    </div>
                                </div>
                                <!--End Thumnail Item-->
                            <?php
                            }

                            //print_r($testimonials);
                            $home_count = 0;
                            foreach($testimonials as $item) {
                                $home_count++;
                                if($home_count <= 20) {
                                    if($main_id != $item['t_id']) {
                                        ?>
                                        <!--Thumnail Item-->
                                        <div class="thumnail-item active clearfix">
                                            <figure class="float-left">
                                                <?php
                                                if ($item['t_imge'] == "") {
                                                    ?>
                                                    <img height="150" width="292"
                                                         src="<?php echo base_url(); ?>assets/images/magazine/img-2.jpg"
                                                         alt="">
                                                <?php } else { ?>
                                                    <img height="150" width="292"
                                                         src="http://adminziwa/assets/uploads/testimonials/<?php echo $item['t_imge']; ?>"
                                                         alt="">
                                                <?php
                                                }
                                                ?>

                                            </figure>
                                            <div class="thumnail-text">
                                                <h4><?php echo $item['t_headline']; ?></h4>
                                                <span><?php echo date('d.m.Y', strtotime($item['t_date'])); ?></span>
                                            </div>
                                        </div>
                                        <!--End Thumnail Item-->
                                    <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <!-- End Magazine Thumnail -->
                </div>
            </div>
        </div>
        <!-- End Magazine Content -->
    </section>
    <!-- End Travel Magazine -->


