<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-4"></div>
    <!--End Background-->

</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn about-page bg-white clearfix">

            <!-- Breakcrumb -->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li>Ziwa Tours Gallery</li>
                </ul>
            </section>
            <!-- End Breakcrumb -->
            <!-- About -->
            <section class="about-cn clearfix">
                <div class="about-text">

                    <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
                    <div id="blueimp-gallery" class="blueimp-gallery">
                        <!-- The container for the modal slides -->
                        <div class="slides"></div>
                        <!-- Controls for the borderless lightbox -->
                        <h3 class="title"></h3>
                        <a class="prev">‹</a>
                        <a class="next">›</a>
                        <a class="close">×</a>
                        <a class="play-pause"></a>
                        <ol class="indicator"></ol>
                        <!-- The modal dialog, which will be used to wrap the lightbox content -->
                        <div class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title"></h4>
                                    </div>
                                    <div class="modal-body next"></div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left prev">
                                            <i class="glyphicon glyphicon-chevron-left"></i>
                                            Previous
                                        </button>
                                        <button type="button" class="btn btn-primary next">
                                            Next
                                            <i class="glyphicon glyphicon-chevron-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="links">
                        <?php
                        foreach($images_map as $image)
                        {
                        ?>
                        <a href="<?php echo base_url(); ?>assets/images/gallery/<?php echo $image;?>" title="Ziwa Tours Gallery" data-gallery>
                            <img height="65" width="65" src="<?php echo base_url(); ?>assets/images/gallery/<?php echo $image;?>" alt="Ziwa Tours Gallery">
                        </a>
                        <?php
                        }
                        ?>
                    </div>

                </div>



        </div>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-image-gallery.min.js"></script>