<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-5"></div>
    <!--End Background-->


</section>
<!--End Banner-->

<!-- Main -->
<div class="main main-dt">
    <div class="container">
        <div class="main-cn detail-page bg-white clearfix">

            <!-- Breakcrumb -->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="#"><i class="fa fa-home"></i></a></li>
                    <li><a href="#" title="">Ziwa</a></li>
                    <li><a href="#" title="">Tours</a></li>
                    <li>Services</li>
                </ul>
                <div class="support float-right">
                    <small>Got a question?</small> +27 21 837 1617
                </div>
            </section>
            <!-- End Breakcrumb -->

            <!-- Header Detail -->
            <section class="head-detail">
                <div class="head-dt-cn">
                    <div class="row">
                        <div class="col-sm-7">
                            <h1>Services</h1>
                        </div>
                        <div class="col-sm-5 text-right">
                            <p class="price-book">
                                <a href="/enquire" title="" class="awe-btn awe-btn-1 awe-btn-lager">Enquire Now</a>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Header Detail -->


            <?php
            $outer_index = 0;
            $image_count = 1;
            foreach($services as $item) {
            ?>
                <!-- Optional Activities -->
                <section class="optional-acitivites detail-cn" id="<?php echo $item['service_id']; ?>">
                    <div class="row">
                        <div class="col-lg-3 detail-sidebar">
                            <div class="scroll-heading">
                                <?php
                                $heading = "";
                                $subheadmings = "";
                                foreach($services as $item_inner) {
                                    if($item['service_name'] == $item_inner['service_name']) {
                                        $heading = '<h2>'.$item_inner['service_name'].'</h2><hr class="hr">';
                                    }else{
                                        $subheadmings .= '<a href="#'.$item_inner['service_id'].'" title="">'.$item_inner['service_name'].'</a>';
                                    }
                                }
                                echo $heading;
                                echo $subheadmings;
                                ?>

                            </div>
                        </div>
                        <div class="col-lg-9 optional-acitivites-cn">
                            <!-- Optional Text -->
                            <div class="tour-description">
                                <h2 class="title-detail">
                                    <?php echo $item['service_name']; ?>
                                </h2>
                                <?php
                                $service_content = preg_replace('/\n{2,}/', "</p><p>", trim($item['service_content']));
                                $service_content = preg_replace('/\n/', '</p><p>', $item['service_content']);
                                $service_content = "<p>{$service_content}</p>";
                                ?>
                                <div class="tour-detail-text">
                                    <?php echo $service_content; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End Optional Activities -->
                <center><img width="450px" height="350px" src="<?php echo base_url(); ?>assets/images\services/<?php echo $image_count.".jpg"; ?>" alt="<?php echo $item['service_name']; ?>" /></center>
            <?php
                $image_count++;
            }
            ?>

            <section class="detail-footer tour-detail-footer detail-cn">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-9 detail-footer-cn text-right">
                        <p class="price-book">
                           <a href="/enquire" title="" class="awe-btn awe-btn-1 awe-btn-lager">Enquire Now</a>
                        </p>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
<!-- End Main -->
