<script src="<?php echo base_url(); ?>assets/js/booking.js"></script>

<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->

</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn bg-white clearfix">
            <div class="step">
                <!-- Step -->
                <ul class="payment-step text-center clearfix">
                    <li class="step-select">
                        <span>1</span>
                        <p>Choose Your Tour</p>
                    </li>
                    <li class="step-part">
                        <span>2</span>
                        <p>Your Booking Details</p>
                    </li>
                    <li>
                        <span>3</span>
                        <p>Booking Reservation Completed!</p>
                    </li>
                </ul>
                <!-- ENd Step -->
            </div>
            <!-- Payment Room -->
            <div class="payment-room">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="payment-info">
                            <h2><?php echo $tour->tour_name; ?></h2>
                                    <span class="star-room">
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                    </span>
                            <ul>


                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="payment-price">

                            <figure>
                                <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $tour->tour_id; ?>/main/<?php echo $tour->tour_main_image; ?>" alt="">
                            </figure>
                            <div class="total-trip">
                                        <span>
                                            Description:
                                        </span>

                                <p>

                                    <i>
                                        <?php echo substr($tour->tour_overview,0,200).'...'; ?>.
                                    </i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Payment Room -->

            <div class="payment-form">
                <form name="ziwa_tour_info">
                <div class="row form">
                    <div class="col-md-6">
                        <h2>Your Information</h2>
                        <input id="tour_id" name="tour_id" value="<?php echo $tour_id; ?>" type="hidden" />
                        <div class="form-field">
                            <input id="firstname" name="firstname" type="text" placeholder="First Name" class="field-input">
                        </div>
                        <div class="form-field">
                            <input id="email" name="email" type="text" placeholder="Email" class="field-input">
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div id="booking_message_block" class="alert-box">
                            <h6>Booking Status</h6>
                            <span>Please complete details to reserve your tour booking.</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6 expiry-date">
                                <label>Tour Booking Date</label>
                                <div class="row form">
                                    <div class="col-xs-6">
                                        <div class="form-field">
                                            <input id="bookdate" name="bookdate" placeholder="Tour Date" type="text" class="field-input calendar-input">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-6 cvc-code">
                                <label>Ziwa Promo Code</label>
                                <div class="form-field">
                                    <input id="bookcode" name="bookcode" placeholder="Ziwa Promo Code (optional)" type="text" class="field-input">
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="submit text-center">
                    <p>
                        By selecting to complete this booking I acknowledge that I have read and accept the <span>rules &amp; restrictions terms &amp; conditions</span> , and <span>privacy policy</span>.
                    </p>

                    <a id="request_tour_booking_link" href="#" class="awe-btn awe-btn-1 awe-btn-medium">Confirm Booking Details</a>

                </div>
                </form>

            </div>
        </div>
    </div>
</div>
<!-- End Main -->