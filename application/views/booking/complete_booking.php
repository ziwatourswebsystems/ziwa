
<!--Banner-->
<section class="sub-banner">
    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->

</section>
<!--End Banner-->

<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn bg-white clearfix">
            <div class="step">
                <!-- Step -->
                <ul class="payment-step text-center clearfix">
                    <li class="step-select">
                        <span>1</span>
                        <p>Choose Your Tour</p>
                    </li>
                    <li class="step-select">
                        <span>2</span>
                        <p>Your Booking Details</p>
                    </li>
                    <li class="step-select">
                        <span>3</span>
                        <p>Booking Reservation Completed!</p>
                    </li>
                </ul>
                <!-- ENd Step -->
            </div>
            <!-- Payment Room -->
            <div class="payment-room">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="payment-info">
                            <h2><?php echo $tour->tour_name; ?></h2>
                                    <span class="star-room">
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                        <i class="glyphicon glyphicon-star"></i>
                                    </span>
                            <ul>


                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="payment-price">

                            <figure>
                                <img src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $tour->tour_id; ?>/main/<?php echo $tour->tour_main_image; ?>" alt="">
                            </figure>
                            <div class="total-trip">
                                        <span>
                                            Description:
                                        </span>

                                <p>

                                    <i>
                                        <?php echo substr($tour->tour_overview,0,200).'...'; ?>.
                                    </i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Payment Room -->
            <div class="main-cn bg-white clearfix">
                <div class="submit text-center">
                    <br />
                    <p>
                        Thank you for placing your booking with Ziwa Tours please check your email with details. If you have any queries or do not recieve the email please contact info@ziwatours.biz for assistance.
                    </p>

                    <a href="/tours" class="awe-btn awe-btn-1 awe-btn-medium">Completed Booking back to Tours</a>
                </div>
        </div>

    </div>
        </div>
    </div>
</div>
<!-- End Main -->