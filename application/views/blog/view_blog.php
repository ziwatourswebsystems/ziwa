<!--Banner-->
<section class="sub-banner">

    <!--Background-->
    <div class="bg-parallax bg-1"></div>
    <!--End Background-->

</section>
<!--End Banner-->
<!-- Main -->
<div class="main">
    <div class="container">
        <div class="main-cn bg-white clearfix">
            <!--Breakcrumb-->
            <section class="breakcrumb-sc">
                <ul class="breadcrumb arrow">
                    <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                    <li>Ziwa Tours Stories</li>
                </ul>
                <!-- Search Blog -->
                <div class="search-blog">
                    <form action="blogsearch">
                        <input type="text" class="search-blog-input" placeholder="To seach type and hit enter">
                        <button class="btn-search-blog">Search</button>
                    </form>
                </div>
                <!-- Search Blog -->
            </section>
            <!--End Breakcrumb-->
            <section class="blog-content">
                <div class="row">

                    <!-- Blog Right -->
                    <div class="col-md-9 col-md-push-3 ">
                        <div class="post-cn">

                            <?php
                            $record_count = 0;
                            $blog_index = 0;

                            foreach($blogs as $item) {

                                if($record_count == 0) {

                                    $blog_index = $item['blog_id'];
                                    ?>
                                    <!-- Post Photo -->
                                    <div class="post">
                                        <div class="post-media">
                                            <div class="image-wrap">
                                                <?php
                                                if ($item['blog_image'] != "") {
                                                    ?>
                                                    <img
                                                        src="<?php echo $this->config->item('member_base_url'); ?>assets/custom/uploads/blog/<?php echo $item['blog_id']; ?>/main/<?php echo $item['blog_image']; ?>"
                                                        alt="">
                                                <?php
                                                } else {
                                                    ?>
                                                    <img src="<?php echo base_url(); ?>assets/images/blog/img-1.jpg"
                                                         alt="">
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="post-text row">
                                            <div class="col-sm-6">
                                                <div class="author-date">
                                                    <a href="">Ziwa Tours</a>
                                                    <small>///</small>
                                                    <span><?php echo date('M, j, Y', strtotime($item['blog_date_created']));?></span>
                                                </div>
                                                <h2><a href="#"><?php echo $item['blog_title']; ?></a></h2>

                                                <div class="post-share">
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://ziwatours.biz/blog/view_blog/<?php echo $item['blog_id']; ?>"
                                                       title=""><i class="fa fa-facebook"></i></a>
                                                    <a href="https://twitter.com/home?status=http://ziwatours.biz/blog/view_blog/<?php echo $item['blog_id']; ?>"
                                                       title=""><i class="fa fa-twitter"></i></a>
                                                    <a href="https://plus.google.com/share?url=http://ziwatours.biz/blog/view_blog/<?php echo $item['blog_id']; ?>"
                                                       title=""><i class="fa fa-google-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">

                                                <?php
                                                $string = preg_replace('/\n{2,}/', "</p><p>", trim($item['blog_body']));
                                                $string = preg_replace('/\n/', '</p><p>', $item['blog_body']);
                                                $string = "<p>{$string}</p>";
                                                ?>
                                                <p><?php
                                                    echo $string;
                                                    ?></p>
                                                <ul class="post-meta">
                                                    <li><a href="">in <span>Winter, Western Cape</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Post Photo -->
                                <?php
                                }
                                $record_count++;
                            }
                            ?>

                            <?php
                            if(count($blog) > 1) {
                                ?>
                                <!-- Navpage Post -->
                                <div class="navpage-post">
                                    <?php
                                    if ($blog_index > 2)
                                    {
                                        ?>
                                        <a href="/blog/view_blog/<?php echo --$blog_index; ?>" class="awe-btn awe-btn-5 awe-btn-lager arrow-left float-left">Prev</a>
                                    <?php
                                    }
                                    ?>

                                    <a href="/blog/view_blog/<?php echo ++$blog_index; ?>" class="awe-btn awe-btn-5 awe-btn-lager arrow-right float-right">Next</a>
                                </div>
                                <!-- End Navpage Post -->
                            <?php
                            }
                            ?>
                        </div>

                    </div>
                    <!-- End Blog Right -->

                    <!-- Blog Left -->
                    <div class="col-md-3 col-md-pull-9">


                        <!-- Widget Recent Post -->
                        <div class="widget widget_recent_entries">
                            <h2 class="title-sidebar">Recent post</h2>
                            <ul>
                                <?php
                                $count = 0;
                                foreach($blog as $item) {
                                    $count++;
                                    if($count < 6) {
                                        ?>
                                        <li>
                                            <a href="/blog/view_blog/<?php echo $item['blog_id']; ?>"><?php echo $item['blog_title']; ?></a><span> / <?php echo $item['blog_tags']; ?></span>
                                        </li>
                                    <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <!-- End Widget Recent post -->
                        <!-- Widget Tags -->
                        <div class="widget widget_tag_cloud">
                            <h2 class="title-sidebar">Tag cloud</h2>
                            <div class="tagcloud">
                                <a href="#Tour">Tour</a>
                                <a href="#News">News</a>
                                <a href="#Stories">Stories</a>
                                <a href="#Package">Package</a>
                                <a href="#Special">Special</a>
                                <a href="#Exclusive">Exclusive</a>
                                <a href="#Competitions">Competitions</a>
                                <a href="#Blog">Blog</a>
                                <a href="#Other">Other</a>
                            </div>
                        </div>
                        <!-- End Widget Tags -->


                    </div>
                    <!-- End Blog Left -->
                </div>
            </section>
        </div>
    </div>
</div>
<!-- End Main --><!-- End Main -->