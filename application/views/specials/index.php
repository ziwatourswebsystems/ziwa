<script src="<?php echo base_url(); ?>assets/js/booking.js"></script>




<!--Banner-->
    <section class="banner">

        <!--Background-->
        <div class="bg-parallax bg-4"></div>
        <!--End Background-->

        <div class="container">





        </div>

    </section>
    <!--End Banner-->

    <!-- Sales -->
    <section class="sales">
        <!-- Title -->
        <div class="title-wrap">
            <div class="container">
                <div class="travel-title float-left">
                    <h2>Hot Deals:</h2>
                </div>
                <div class="support float-right">
                    <small>Got a question?</small> +27 82 389 0895
                </div>
            </div>
        </div>
        <!-- End Title -->
        <!-- Hot Sales Content -->
        <div class="container">
            <div class="sales-cn">
                <div class="row">
                    <?php

                    foreach($specials as $item) {
                        ?>
                        <!-- HostSales Item -->
                        <div class="col-xs-6 col-md-3">
                            <div class="sales-item">
                                <figure class="home-sales-img">
                                    <a href="" title="">
                                        <?php
                                        if($item['tour_main_image'] != "")
                                        {
                                            ?>
                                            <img height="292" width="292"  src="<?php echo $this->config->item('member_base_url');  ?>assets/custom/uploads/tours/<?php echo $item['tour_id']; ?>/main/<?php echo $item['tour_main_image']; ?>" alt="">
                                        <?php
                                        }else {
                                            ?>
                                            <img src="<?php echo base_url(); ?>assets/images//ziwatours.png" alt="">
                                        <?php
                                        }
                                        ?>
                                    </a>

                                </figure>
                                <div class="home-sales-text">
                                    <div class="home-sales-name-places">
                                        <div class="home-sales-name">
                                            <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" title=""><?php echo urldecode($item['tour_name']); ?></a>
                                        </div>
                                        <div class="home-sales-places">
                                            <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" title="">  <?php echo urldecode(substr($item['tour_overview'],0,40).'...'); ?></a>
                                        </div>
                                    </div>
                                    <hr class="hr">
                                    <div class="price-box">
                                        <a href="/tours/view_tour/<?php echo $item['tour_id']; ?>" class="awe-btn awe-btn-4 arrow-right awe-btn-small">View
                                            Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End HostSales Item -->
                    <?php
                    }
                    ?>

                </div>
            </div>
        </div>
        <!-- End Hot Sales Content -->
    </section>
    <!-- End Sales -->



    <!-- Travel Magazine -->
    <section class="magazine">
        <!-- Title -->
        <div class="title-wrap">
            <div class="container">
                <div class="travel-title float-left">
                    <h2>Customer Testimonials</h2>
                </div>
                <a href="#" title="" class="awe-btn awe-btn-5 arrow-right awe-btn-lager text-uppercase float-right">view all</a>
            </div>
        </div>
        <!-- End Title -->
        <!-- Magazine Content -->
        <div class="container">
            <div class="magazine-cn">
                <div class="row">
                    <!-- Magazine Descript -->
                    <div class="col-lg-6">
                        <div class="magazine-ds">
                            <div id="owl-magazine-ds">
                                <!-- Magazine Descript Item -->
                                <?php
                                //print_r($testimonials);
                                $home_count = 0;
                                foreach($testimonials as $item) {
                                    $home_count++;
                                    if($home_count <= 4) {


                                        ?>
                                        <!-- Magazine Descript Item -->
                                        <div class="magazine-item">
                                            <div class="magazine-header">
                                                <h2><?php echo urldecode($item['t_headline']); ?></h2>
                                                <ul>
                                                    <li>by <a href=""
                                                              title=""><?php echo ucwords($item['t_author']); ?></a>
                                                    </li>
                                                    <li><?php echo date('d.m.Y', strtotime($item['t_date']));?></li>
                                                </ul>
                                                <hr class="hr">
                                            </div>
                                            <?php
                                            $string = preg_replace('/\n{2,}/', "</p><p>", trim($item['t_body']));
                                            $string = preg_replace('/\n/', '</p><p>', $item['t_body']);
                                            $string = "<p>{$string}</p>";
                                            ?>
                                            <div class="magazine-body">
                                                <?php echo urldecode($string); ?>
                                            </div>
                                            <div class="magazine-footer clearfix">
                                                <div class="post-share magazine-share float-left">
                                                    <a data-t-id="<?php echo $item['t_id']; ?>"
                                                       href="https://www.facebook.com/sharer/sharer.php?u=http://ziwatours.biz/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                       title=""><i class="fa fa-facebook"></i></a>
                                                    <a data-t-id="<?php echo $item['t_id']; ?>"
                                                       href="https://twitter.com/home?status=http://ziwatours.biz/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                       title=""><i class="fa fa-twitter"></i></a>
                                                    <a data-t-id="<?php echo $item['t_id']; ?>"
                                                       href="https://plus.google.com/share?url=http://ziwatours.biz/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                       title=""><i class="fa fa-google-plus"></i></a>
                                                </div>
                                                <a href="/home/view_testimonials/<?php echo $item['t_id']; ?>"
                                                   title=""
                                                   class="awe-btn awe-btn-5 arrow-right awe-btn-lager text-uppercase float-right">view
                                                    more</a>
                                            </div>
                                        </div>
                                        <!-- End Magazine Descript Item -->
                                    <?php

                                    }
                                }
                                ?>
                                <!-- End Magazine Descript Item -->

                            </div>
                        </div>
                    </div>
                    <!-- End Magazine Descript -->
                    <!-- Magazine Thumnail -->
                    <div class="col-lg-6">
                        <div class="magazine-thum" id="magazine-thum">
                            <!--Thumnail Item-->
                            <?php
                            //print_r($testimonials);
                            $home_count = 0;
                            foreach($testimonials as $item) {
                                $home_count++;
                                if($home_count <= 4) {
                                    ?>
                                    <!--Thumnail Item-->
                                    <div class="thumnail-item active clearfix">
                                        <figure class="float-left">
                                            <?php
                                            if($item['t_imge'] == "") {
                                                ?>
                                                <img height="150" width="292" src="<?php echo base_url(); ?>assets/images/ziwatours.png" alt="">
                                            <?php }else{ ?>
                                                <img height="150" width="292" src="<?php echo $this->config->item('member_base_url');  ?>assets/uploads/testimonials/<?php echo $item['t_imge']; ?>" alt="">
                                            <?php
                                            }
                                            ?>

                                        </figure>
                                        <div class="thumnail-text">
                                            <h4><?php echo $item['t_headline']; ?></h4>
                                            <span><?php echo date('d.m.Y',strtotime($item['t_date']));?></span>
                                        </div>
                                    </div>
                                    <!--End Thumnail Item-->
                                <?php
                                }
                            }
                            ?>
                            <!--End Thumnail Item-->
                        </div>
                    </div>
                    <!-- End Magazine Thumnail -->
                </div>
            </div>
        </div>
        <!-- End Magazine Content -->
    </section>
    <!-- End Travel Magazine -->

    <!-- Confidence and Subscribe  -->
    <section class="confidence-subscribe">
        <!-- Background -->
        <div class="bg-parallax bg-5"></div>
        <!-- End Background -->
        <div class="container">
            <div class="row cs-sb-cn">

                <!-- Confidence -->
                <div class="col-md-6">
                    <div class="confidence">
                        <h3>Our Partners</h3>
                        <table>
                            <tr >
                                <td style="padding:10px;"> <a href="http://www.capetown.travel/"><img  width="150px" height="150px"  src="<?php echo base_url(); ?>assets/partners/capetowntourism.jpeg" /></a></td>
                                <td style="padding:10px;"> <a href="http://www.satsa.com/" ><img width="150px" height="150px" src="<?php echo base_url(); ?>assets/partners/satsa.jpg" /></a></td>
                                <td style="padding:10px;"> <a href="http://www.tripadvisor.co.za/Attraction_Review-g312659-d6124159-Reviews-Ziwas_Cape_Touring_Day_Tours-Cape_Town_Central_Western_Cape.html" ><img  width="250px" height="80px" src="<?php echo base_url(); ?>assets/partners/tripadvisor.jpg" /></a></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- End Confidence -->
                <!-- Subscribe -->
                <div class="col-md-6">
                    <div class="subscribe">
                        <h3>Subscribe to our newsletter</h3>
                        <div id="subscriber_success_message" class="alert-box" style="display:none">
                            <h6 id="subscribe_heading">Subscriber Success Message</h6>
                            <span id="subscribe_msg">Congratulations <b id="pksm_name"></b> you have provisionally completed your booking request. Please check the email we sent to <b id="pksm_email"></b> for booking request confirmation details and instructions.Thank you for choosing Ziwa Tours one of our frinedly consultant will be in touch shortly</span>
                        </div>

                        <p>Enter your email address and we’ll send you our regular promotional emails, packed with special offers, great deals, and huge discounts</p>
                        <!-- Subscribe Form -->
                        <div class="subscribe-form">
                            <form action="#" method="get">
                                <input id="email_subscriber_fld" type="text" name="" value="" placeholder="Your email" class="subscribe-input">
                                <button id="btn_subscribe_add" type="submit" class="awe-btn awe-btn-5 arrow-right text-uppercase awe-btn-lager">subcrible</button>
                            </form>
                        </div>
                        <!-- End Subscribe Form -->
                        <!-- Follow us -->
                        <div class="follow-us">
                            <h4>Follow us</h4>
                            <div class="follow-group">
                                <a href="https://www.facebook.com/pages/Ziwascape-touring/133572513517134" title=""><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/ZiwasCapetourin" title=""><i class="fa fa-twitter"></i></a>

                            </div>
                        </div>
                        <!-- Follow us -->
                    </div>
                </div>
                <!-- End Subscribe -->

            </div>
        </div>
    </section>
    <!-- End Confidence and Subscribe  -->

