<?php

class home_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

	//Validates if user exists

    public function get_main_testimonials($main_id)
    {
        $this->db->from('testimonials');
        $this->db->where('t_id', $main_id);
        $query = $this->db->get();
        return $query->row();
    }

    public function get_all_tours()
    {
        $this->db->from('tours');
        $this->db->order_by("last_updated", "desc");
        $query = $this->db->get();
        return $query->result_array ();
    }


    public function get_all_exclusives()
    {
        $this->db->from('tours');
        $this->db->where('is_exclusive', 1);
        $this->db->order_by("last_updated", "desc");
        $query = $this->db->get();
        return $query->result_array ();
    }


    public function get_all_packages()
    {
        $this->db->from('service_types');
        $this->db->where('parent', 2);
        $this->db->order_by("date_created", "asc");
        $query = $this->db->get();
        return $query->result_array ();
    }

    public function get_all_specials()
    {
        $this->db->from('tours');
        $this->db->where('is_special', 1);
        $this->db->order_by("last_updated", "desc");
        //$this->db->limit(4);
        $query = $this->db->get();
        return $query->result_array ();
    }

    public function get_all_testimonials()
    {
        $this->db->from('testimonials');
        $this->db->order_by("t_date", "desc");
        $this->db->limit(20);
        $query = $this->db->get();
        return $query->result_array ();
    }


    public function create_booking_request($bname,$email,$btype,$bid,$bdate)
    {
        $transaction_tokem = random_string('alnum',20);
        $fdate=date('Y-m-d',strtotime($bdate));
        $data = array(
            'booking_type_id' => $btype,
            'booking_id' =>	$bid,
            'user_id' => $email,
            'date_booked_for' => $fdate,
            'status' => 0,
            'transaction_token' => $transaction_tokem
        );
        $this->db->insert('booking_transaction', $data);
        // get the last inserted id
        $transaction_id = $id = $this->db->insert_id();
        $transaction_hash = $transaction_id."_".$transaction_tokem;
        $url = "members.ziwatours.biz/members/view_booking/".$transaction_hash;
        //$this->send_booking_reservation_email($email,$bname,$url);
    }

    public function send_booking_reservation_email($email,$user_name,$url)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$user_name."</p>";
        $message .= "<p>Thank you for choosing us as your travel guide. Your booking has been provisionally placed.</p>";
        $message .= "<p>Please check your member dashboard for full control of the booking.</p>";
        $message .= "<p>You can view the detail of your booking here ".$url."</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Ziwa Tours Booking Reservation');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

}