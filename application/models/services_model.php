<?php

class Services_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }


    public function get_all_services()
    {
        $this->db->from('service_types');
        $this->db->order_by("service_id", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function send_booking_reservation_email($email,$user_name,$url)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$user_name."</p>";
        $message .= "<p>Thank you for choosing us as your travel guide. Your booking has been provisionally placed.</p>";
        $message .= "<p>Please check your member dashboard for full control of the booking.</p>";
        $message .= "<p>You can view the detail of your booking here ".$url."</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Ziwa Tours Booking Reservation');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

}