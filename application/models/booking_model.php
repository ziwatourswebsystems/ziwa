<?php

class booking_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function add_subscriber($email)
    {
        $data = array(
            'subscriber_email' => $email,
            'subsctibe_value' => 1
        );

        $this->db->insert('subscribers', $data);
    }

	//Validates if user exists
    public function increment_stats($stat_type_id)
    {
        echo "type : ".$stat_type_id;
        // increment global figures
        if($stat_type_id == 1) {
            $this->db->set("private", "private + 1", FALSE);
        }
        elseif($stat_type_id == 2) {
            $this->db->set("packages", "packages + 1", FALSE);
        }
        elseif($stat_type_id == 3) {
            $this->db->set("tours", "tours + 1", FALSE);
        }
        elseif($stat_type_id == 4) {
            $this->db->set("exclusive", "exclusive + 1", FALSE);
        }
        $this->db->update('ziwa_overall_stats');

        $curYear = date('Y');
        $curMonth = date('m');

        $this->db->where('stat_month',$curMonth);
        $this->db->where('stat_year',$curYear);
        $q = $this->db->get('ziwa_grouped_stats');

        if ( $q->num_rows() == 0 )
        {
            $data = array(
                'stat_month' => $curMonth,
                'stat_year' => $curYear
            );
            $this->db->insert('ziwa_grouped_stats', $data);
        }

        // increment monthly figures
        if($stat_type_id == 1) {
            $this->db->set("private", "private + 1", FALSE);
        }
        elseif($stat_type_id == 2) {
            $this->db->set("packages", "packages + 1", FALSE);
        }
        elseif($stat_type_id == 3) {
            $this->db->set("tours", "tours + 1", FALSE);
        }
        elseif($stat_type_id == 4) {
            $this->db->set("exclusive", "exclusive + 1", FALSE);
        }
        $this->db->where('stat_month', $curMonth);
        $this->db->where('stat_year', $curYear);
        $this->db->update('ziwa_grouped_stats');

    }


    public function create_booking_request($bname,$email,$btype,$bid,$bdate)
    {
        $transaction_tokem = random_string('alnum',20);
        $fdate=date('Y-m-d',strtotime($bdate));
        $data = array(
            'booking_type_id' => $btype,
            'booking_id' =>	$bid,
            'user_id' => $email,
            'date_booked_for' => $fdate,
            'status' => 0,
            'transaction_token' => $transaction_tokem
        );
        $this->db->insert('booking_transaction', $data);
        // get the last inserted id
        $transaction_id = $id = $this->db->insert_id();
        $transaction_hash = $transaction_id."_".$transaction_tokem;
        $url = "http://members.ziwatours.biz/members/view_booking/".$transaction_hash;
        $this->increment_stats($btype);
        $this->send_booking_reservation_email($email,$bname,$url);
    }

    public function send_enquiry($subject,$ref,$name,$email,$user_message)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $formatted_subject = "";

        if($ref == "")
        {
            $formatted_subject = $subject;

        }
        else
        {
            $formatted_subject = $subject." Ref : ".$ref;
        }

        $this->load->library('email', $config);
        $message = "<p>Hi Ziwa Tours From : ".$name." Email '.$email.'</p>";
        $message .= "<p>".$user_message."</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz',$name); // change it to yours
        $this->email->to($this->config->item('admin_email'));// change it to yours
        $this->email->subject('Ziwa Enquiry - '.$formatted_subject);
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

    public function send_booking_reservation_email($email,$user_name,$url)
    {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.ziwatours.biz',
            'smtp_port' => 26,
            'smtp_user' => 'info@ziwatours.biz', // change it to yours
            'smtp_pass' => 'ziwamail100', // change it to yours
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );

        $this->load->library('email', $config);
        $message = "<p>Hi ".$user_name."</p>";
        $message .= "<p>Thank you for choosing us as your travel guide. Your booking has been provisionally placed.</p>";
        $message .= "<p>Please check your member dashboard for full control of the booking.</p>";
        $message .= "<p>You can view the detail of your booking here ".$url."</p>";
        $message .= "<p>Kind Regards,</p>";
        $message .= "<p>Ziwa Tours Team</p>";

        $this->email->set_newline("\r\n");
        $this->email->from('info@ziwatours.biz','Ziwa Tours Team'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject('Ziwa Tours Booking Reservation');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            echo "Email sending error";
        }
    }

}