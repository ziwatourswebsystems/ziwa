<?php

class tour_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function countTours()
    {
        $table_row_count = $this->db->count_all('tours');
        return $table_row_count;
    }

    public function get_all_tours()
    {
        $this->db->from('tours');
        $this->db->order_by("last_updated", "desc");
        $query = $this->db->get();
        return $query->result_array ();
    }

    public function get_tour_details($tour_id)
    {
        $this->db->from('tours');
        $this->db->where('tour_id', $tour_id);
        $query = $this->db->get();
        return $query->row();
    }

}