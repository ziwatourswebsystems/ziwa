<?php

class blog_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string');
    }

    public function get_all_blogs()
    {
        $this->db->from('blog');
        $this->db->order_by("blog_date_created", "desc");
        $query = $this->db->get();
        return $query->result_array ();
    }

    public function get_blog_details($blog_id)
    {
        $this->db->from('blog');
        $this->db->where('blog_id', $blog_id);
        $query = $this->db->get();
        return $query->result_array ();
    }


    public function get_tour_details($tour_id)
    {
        $this->db->from('tours');
        $this->db->where('tour_id', $tour_id);
        $query = $this->db->get();
        return $query->row();
    }

}